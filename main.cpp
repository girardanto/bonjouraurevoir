#include "Monde.h"
#include "PollueurToutDroit.h"
#include "PollueurSauteur.h"
#include "RobotNettoyeur.h"
#include "NettoyeurDistrait.h"

int main() {
    Monde *monde = new Monde(10, 10);

    monde->metPapierGras(1,3);
    monde->metPapierGras(3,6);

    if (monde->estSale(1,2)) {
        cout << "La case (1,2) est sale" << endl;
    }

    cout << "Il y a " << monde->nbPapierGras() << " papier(s) "
            "gras dans ce monde." << endl;

    PollueurToutDroit *robotCaca = new PollueurToutDroit(1,1, monde,9);

    robotCaca->parcourir();

    PollueurSauteur* robotPipi = new PollueurSauteur(0,0,monde,4);

    robotPipi->parcourir();

    monde->afficherMatrice();

    cout << "==========" << endl;

    NettoyeurDistrait* robotEncule = new NettoyeurDistrait(monde);

    robotEncule->parcourir();

   /* RobotNettoyeur* robotGentil = new RobotNettoyeur(monde);


    robotGentil->parcourir();*/

    monde->afficherMatrice();


    std::cin.ignore(); // wait for input
    return EXIT_SUCCESS; // #Norme EISTI
}