//
// Created by girardanto on 27/04/17.
//

#include "RobotPollueur.h"

RobotPollueur::RobotPollueur() {}

RobotPollueur::RobotPollueur(int x, int y, Monde* m) : Robot(x, y, m) {
}

void RobotPollueur::polluer() {
    getMonde()->metPapierGras(getPosx(), getPosy());
}
