//
// Created by girardanto on 27/04/17.
//

#ifndef TP5_BIS_ROBOTNETTOYEUR_H
#define TP5_BIS_ROBOTNETTOYEUR_H


#include "Robot.h"

class RobotNettoyeur: public Robot {
public:
    RobotNettoyeur(Monde* monde);

    RobotNettoyeur();

    void nettoyer();
    void parcourir();
};


#endif //TP5_BIS_ROBOTNETTOYEUR_H
