//
// Created by girardanto on 27/04/17.
//

#include "PollueurToutDroit.h"

PollueurToutDroit::PollueurToutDroit(int x, int y, Monde* m, int depart) : RobotPollueur(x, y, m) {

    if (getMonde()->caseExiste(0, colDepart)) {
        colDepart = depart;
    } else {
        colDepart = 0;
    }
}

void PollueurToutDroit::parcourir() {
    allerEn(0, colDepart);

    for (int i = 0; i < getMonde()->getNbL(); ++i) {
        allerEn(i, colDepart);
        polluer();
    }
}
