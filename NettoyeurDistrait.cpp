//
// Created by girardanto on 27/04/17.
//

#include "NettoyeurDistrait.h"

NettoyeurDistrait::NettoyeurDistrait(Monde* monde): RobotNettoyeur(monde) {

}

void NettoyeurDistrait::parcourir() {
    for (int i = 0; i < getMonde()->getNbL(); ++i) {
        for (int j = 0; j < getMonde()->getNbC(); ++j) {
            allerEn(i,j);
            if (((i + j) % 2) == 1)
                nettoyer();
        }
    }
}
