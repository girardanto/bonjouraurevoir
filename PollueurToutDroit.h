//
// Created by girardanto on 27/04/17.
//

#ifndef TP5_BIS_POLLUERTOUTDROIT_H
#define TP5_BIS_POLLUERTOUTDROIT_H


#include "RobotPollueur.h"

class PollueurToutDroit: public RobotPollueur {
private:
    int colDepart;
public:
    PollueurToutDroit(int x, int y, Monde* m, int depart);
    void parcourir();
};


#endif //TP5_BIS_POLLUERTOUTDROIT_H
