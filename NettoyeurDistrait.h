//
// Created by girardanto on 27/04/17.
//

#ifndef TP5_BIS_NETTOYEURDISTRAIT_H
#define TP5_BIS_NETTOYEURDISTRAIT_H


#include "RobotNettoyeur.h"

class NettoyeurDistrait: public RobotNettoyeur {
public:
    NettoyeurDistrait(Monde* monde);
    void parcourir();
};


#endif //TP5_BIS_NETTOYEURDISTRAIT_H
