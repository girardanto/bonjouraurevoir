//
// Created by girardanto on 27/04/17.
//

#ifndef TP5_BIS_MONDE_H
#define TP5_BIS_MONDE_H

#include <iostream>
using std::cout;
using std::endl;

class Monde {
private:
    int nbL;
    int nbC;
    bool **mat;

public:
    Monde();
    Monde(int l, int c);

    void afficherMatrice();
    void metPapierGras(int i, int j);
    void prendPapierGras(int i, int j);
    bool estSale(int i, int j);
    bool caseExiste(int i, int j);
    int nbPapierGras();

    int getNbL() const;
    int getNbC() const;
};


#endif //TP5_BIS_MONDE_H
