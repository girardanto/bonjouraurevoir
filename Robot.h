//
// Created by girardanto on 27/04/17.
//

#ifndef TP5_BIS_ROBOT_H
#define TP5_BIS_ROBOT_H

#include "Monde.h"


class Robot {
private:
    int posx;
    int posy;
    Monde *monde;

protected:
    Robot();
    Robot(int x, int y, Monde* monde);
    void allerEn(int x, int y);
    virtual void parcourir();

    int getPosx() const;

    int getPosy() const;

    Monde * getMonde() const;
};

#endif //TP5_BIS_ROBOT_H
