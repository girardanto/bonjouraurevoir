//
// Created by girardanto on 27/04/17.
//

#ifndef TP5_BIS_POLLUEURSAUTEUR_H
#define TP5_BIS_POLLUEURSAUTEUR_H


#include "RobotPollueur.h"

class PollueurSauteur: public RobotPollueur {
private:
    int deltax;
public:
    PollueurSauteur(int x, int y, Monde* m, int d);
    void parcourir();
};


#endif //TP5_BIS_POLLUEURSAUTEUR_H
