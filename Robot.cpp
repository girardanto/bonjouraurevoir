//
// Created by girardanto on 27/04/17.
//

#include "Robot.h"

Robot::Robot() {}

Robot::Robot(int x, int y, Monde* m) {
    monde = m;

    if (monde->caseExiste(x, y)) {
        posx = x;
        posy = y;
    } else {
        posx = 0;
        posy = 0;
    }
}

void Robot::allerEn(int x, int y) {
    if (monde->caseExiste(x, y)) {
        posx = x;
        posy = y;
    }
}

int Robot::getPosx() const {
    return posx;
}

int Robot::getPosy() const {
    return posy;
}

Monde* Robot::getMonde() const {
    return monde;
}

void Robot::parcourir() {}


