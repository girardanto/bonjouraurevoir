//
// Created by girardanto on 27/04/17.
//

#ifndef TP5_BIS_ROBOTPOLLUEUR_H
#define TP5_BIS_ROBOTPOLLUEUR_H


#include "Robot.h"

class RobotPollueur: public Robot {
protected:
    RobotPollueur();
    RobotPollueur(int x, int y, Monde* m);

    void polluer();
};


#endif //TP5_BIS_ROBOTPOLLUEUR_H
