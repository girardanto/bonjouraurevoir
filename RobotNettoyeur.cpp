//
// Created by girardanto on 27/04/17.
//

#include "RobotNettoyeur.h"

RobotNettoyeur::RobotNettoyeur(Monde *monde) : Robot(0, 0, monde) {

}

void RobotNettoyeur::nettoyer() {
    getMonde()->prendPapierGras(getPosx(), getPosy());
}

void RobotNettoyeur::parcourir() {
    for (int i = 0; i < getMonde()->getNbL(); ++i) {
        for (int j = 0; j < getMonde()->getNbC(); ++j) {
            allerEn(i,j);
            nettoyer();
        }
    }
}

RobotNettoyeur::RobotNettoyeur() {}
