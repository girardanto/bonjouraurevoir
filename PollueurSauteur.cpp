//
// Created by girardanto on 27/04/17.
//

#include "PollueurSauteur.h"

PollueurSauteur::PollueurSauteur(int x, int y, Monde *m, int d) : RobotPollueur(x, y, m) {
    deltax = d;
}

void PollueurSauteur::parcourir() {
    allerEn(0, getPosy());
    polluer();

    for (int i = 1; i < getMonde()->getNbL(); ++i) {
        if (getPosy() + deltax < getMonde()->getNbL()) {
            allerEn(i, getPosy() + deltax);
        } else {
            allerEn(i, 0);
        }
        polluer();
    }
}
