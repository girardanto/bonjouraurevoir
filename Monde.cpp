//
// Created by girardanto on 27/04/17.
//

#include "Monde.h"

Monde::Monde() {}

Monde::Monde(int l, int c) {
    nbL = l;
    nbC = c;

    mat = new bool* [nbL]; // Init 2d-array
    for (int i = 0; i < nbL; ++i) {
        mat[i] = new bool[nbC]; // Init a column

        for (int j = 0; j < nbC; ++j) {
            mat[i][j] = false; // Set to false
        }
    }
}

void Monde::afficherMatrice() {
    for (int i = 0; i < nbL; ++i) {
        for (int j = 0; j < nbC; ++j) {
            if (mat[i][j]) {
                cout << "o";
            } else {
                cout << ".";
            }
        }
        cout << endl;
    }
}

void Monde::metPapierGras(int i, int j) {
    if (caseExiste(i, j)) {
        mat[i][j] = true;
    }
}

void Monde::prendPapierGras(int i, int j) {
    if (caseExiste(i, j)) {
        mat[i][j] = false;
    }
}

bool Monde::estSale(int i, int j) {
    if (caseExiste(i, j)) {
        return mat[i][j];
    }
}

bool Monde::caseExiste(int i, int j) {
    return i >= 0 && i < nbL && j >= 0 && j < nbC;
}

int Monde::nbPapierGras() {
    int count = 0;
    for (int i = 0; i < nbL; ++i) {
        for (int j = 0; j < nbC; ++j) {
            if (estSale(i, j))
                count++;
        }
    }
    return count;
}

int Monde::getNbL() const {
    return nbL;
}

int Monde::getNbC() const {
    return nbC;
}


